// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;
import 'package:mybloc/model/http_exceptopn.dart';


import 'package:shared_preferences/shared_preferences.dart';

class DBHelper {
  Future<String> authuser(
      String email, String password, String username, String urlsegment) async {
    final url =
        'https://identitytoolkit.googleapis.com/v1/accounts:$urlsegment?key=AIzaSyBpNskK7VW1EsbXdHj0OhC_uRstYqY5-YU ';
    try {
      final res = await http.post(Uri.parse(url),
          body: jsonEncode({
            'email': email,
            'password': password,
            'returnSecureToken': true,
          }));

      final data = await jsonDecode(res.body);
        if (data['error'] != null) {
        print(data['error']['message']);
        print('ooooooooooooooooooooooooooooooooooo');
        throw HttpException(data['error']['message']);
      }
      final String id = data['localId'];
      addusertodb(id, email, password, username);
      
      

    
      final pref = await SharedPreferences.getInstance();
      String userdata = jsonEncode({
        'userid': id,
        'useremail': email,
      });
      pref.setString('userdata', userdata);
     

      return id;
    } catch (e) {
      print('$e errrrrrrrrrooooooorrrrrr');
      throw e;
    }
  }

  Future addusertodb(
      String id, String email, String password, String username) async {
    FirebaseFirestore.instance.collection('Users').doc(id).set({
      'email': email,
      'password': password,
      'username': username,
    });
  }

  Future<void> sendmessage(String message, String id, String useremail) async {
    FirebaseFirestore.instance.collection('messages').doc().set({
      'userid': id,
      'useremail': useremail,
      'message': message,
      'createdAt': Timestamp.now()
    });
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getmsg() {
    return FirebaseFirestore.instance
        .collection('messages')
        .orderBy('createdAt', descending: false)
        .snapshots();
  }

  getid() async {
    
    final pref = await SharedPreferences.getInstance();
    if(!pref.containsKey('userdata')){
      return '';
    }
    final data = jsonDecode(pref.getString('userdata')!);
    return data['userid']!.toString();
  }
  logout()async{
    final pref=await SharedPreferences.getInstance();
    pref.clear();
    

    
  }

}