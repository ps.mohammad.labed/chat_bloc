class UserData {
  String username;
  String email;
  String password;

  String id;

  UserData(
    this.id,
    this.username,
    this.email,
    this.password,
  );
}
