import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mybloc/controller/bloc/signing_bloc.dart';
import 'package:mybloc/view/sign_screen.dart';

import '../controller/bloc/chattingscreen_bloc.dart';
import 'widget/chat_screen_widgets/chat_bubbles.dart';
import 'widget/chat_screen_widgets/send_message.dart';

class ChattingScreen extends StatelessWidget {
  static const routename = '/chat_screen';
  const ChattingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final chattingbloc = BlocProvider.of<ChattingscreenBloc>(context);
    final signbloc = BlocProvider.of<SigningBloc>(context);

    return BlocConsumer<SigningBloc, SigningState>(
      listener: (context, state) {
       if(state is SigningLoading){
          const Center(
            child: CircularProgressIndicator(),
          );
        }
        if(state is LogedOut){
          Navigator.pushReplacement(context, MaterialPageRoute(builder: ((context) =>const  SignScreen())));
        }
      },
      builder: (context, state) {
        return StreamBuilder(
            stream: chattingbloc.getmessages(),
            builder: ((context, snapshot) {
              return !snapshot.hasData
                  ? const Scaffold(
                      body: Center(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : Scaffold(
                      appBar: AppBar(
                        actions: [
                          IconButton(
                              onPressed: () {
                                signbloc.add(Logout());
                              },
                              icon: const Icon(Icons.logout))
                        ],
                        title: const Center(child: Text('Chatting Screen')),
                      ),
                      body: Column(
                        children: [
                          Expanded(
                              child: ListView.builder(
                                  itemCount: snapshot.data!.docs.length,
                                  itemBuilder: ((context, index) => ChatBubbles(
                                      snapshot.data!.docs[index]['message'],
                                      snapshot.data!.docs[index]['useremail'],
                                      signbloc.id ==
                                              snapshot.data!.docs[index]
                                                  ['userid']
                                          ? true
                                          : false)))),
                          SendMessage(),
                        ],
                      ),
                    );
            }));
      },
    );
  }
}
