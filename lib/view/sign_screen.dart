// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mybloc/controller/bloc/signing_bloc.dart';
import 'package:mybloc/view/widget/sign_screen_widgets/build_sign_card.dart';
import 'package:mybloc/view/widget/sign_screen_widgets/build_up_icon_sign_sc.dart';

import 'chatting_screen.dart';

class SignScreen extends StatelessWidget {
  const SignScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final bloc=BlocProvider.of<SigningBloc>(context);
    return BlocConsumer<SigningBloc, SigningState>(
      listener: (context, state) {
        if (state is SigningSuccess) {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: ((context) => const ChattingScreen())));
        }
        if (state is SigningLoading) {
          Future.delayed(Duration(seconds: 5));
          buildprogressindicator();
        }
        if (state is SigningFailed) {
          builderrorsnackbar(context,bloc.errormsg);
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                const BuildTopSignIcon(),
                BuildSignCard(),
              ],
            ),
          ),
        );
      },
    );
  }

  buildprogressindicator() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  builderrorsnackbar(BuildContext context,String error) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        error,
        style: Theme.of(context).textTheme.labelSmall,
      ),
      backgroundColor: Theme.of(context).primaryColor,
    ));
  }
}
