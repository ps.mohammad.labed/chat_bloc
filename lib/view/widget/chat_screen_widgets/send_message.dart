// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../controller/bloc/chattingscreen_bloc.dart';

class SendMessage extends StatelessWidget {
  SendMessage({super.key});
  final _messagectrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ChattingscreenBloc>(context);
    return Align(
      alignment: Alignment.bottomCenter,
      child: SizedBox(
        //height:size.height*0.08 ,
        child: Row(children: [
          buildsendmessagefield(context),
          IconButton(
              onPressed: () {
                print('==================================');
                bloc.add(SendTheMessage());
                _messagectrl.clear();
                FocusScope.of(context).unfocus();
              },
              icon: Icon(
                Icons.send,
                color: Theme.of(context).primaryColor,
              ))
        ]),
      ),
    );
  }

  Widget buildsendmessagefield(BuildContext context) {
    final bloc = BlocProvider.of<ChattingscreenBloc>(context);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          onChanged: (value) => bloc.message = _messagectrl.text,
          controller: _messagectrl,
          onSubmitted: (value) => bloc.message = _messagectrl.text,
          decoration: InputDecoration(
            label: const Text(
              'Send Message ...',
              style: TextStyle(color: Colors.teal),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: BorderSide(
                    color: Theme.of(context).primaryColor, width: 2)),
          ),
        ),
      ),
    );
  }
}
