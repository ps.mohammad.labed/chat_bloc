import 'package:flutter/material.dart';

class ChatBubbles extends StatelessWidget {
  const ChatBubbles(this.message, this.useremail, this.isme, {super.key});
  final bool isme;
  final String message;
  final String useremail;

  @override
  Widget build(BuildContext context) {
    return buildchatcontainer(message, useremail, isme, context);
  }

  buildchatcontainer(
      String message, String useremail, bool isme, BuildContext context) {
    return Row(
      mainAxisAlignment: isme ? MainAxisAlignment.start : MainAxisAlignment.end,
      children: [
        Container(
            width: 150,
            padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
            margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: BoxDecoration(
                color: isme ? Theme.of(context).primaryColor : Colors.grey,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  topRight: const Radius.circular(10),
                  bottomLeft: isme
                      ? const Radius.circular(0)
                      : const Radius.circular(10),
                  bottomRight: isme
                      ? const Radius.circular(10)
                      : const Radius.circular(0),
                )),
            child: Column(
              children: [
                Text(
                  useremail,
                  style: TextStyle(
                      color: isme ? Colors.white : Colors.black,
                      fontWeight: FontWeight.w800),
                ),
                Text(
                  message,
                  style: TextStyle(color: isme ? Colors.white : Colors.black),
                ),
              ],
            )),
      ],
    );
  }
}
