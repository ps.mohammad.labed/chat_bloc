import 'package:flutter/material.dart';

class BuildTopSignIcon extends StatelessWidget {
  const BuildTopSignIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 150,
        height: 150,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        ),
        child: Center(
          child: Icon(
            Icons.person,
            size: 65,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }
}
