// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../controller/bloc/chattingscreen_bloc.dart';

import '../../../controller/bloc/signing_bloc.dart';

class BuildSignCard extends StatelessWidget {
  BuildSignCard({super.key});
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  final _passwordcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final issignin = BlocProvider.of<SigningBloc>(context).issignin;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 26),
      child: Card(
        elevation: 4,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
              key: _formkey,
              child: Column(
                children: [
                 
                  buildtextformfield(context, "E-mail", Icons.email),
                  const SizedBox(
                    height: 15,
                  ),
                  issignin?Container():
                  buildtextformfield(context, "Username", Icons.person),
                  
                  const SizedBox(
                    height: 15,
                  ),
                  buildtextformfield(context, "Password", Icons.remove_red_eye),
                  const SizedBox(
                    height: 15,
                  ),
                  buildbuttonsrow(context, issignin)
                ],
              )),
        ),
      ),
    );
  }

  Widget buildbuttonsrow(BuildContext context, bool issignin) {
    final bloc = BlocProvider.of<SigningBloc>(context);
   
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ElevatedButton(
          onPressed: () {
            
            if (_formkey.currentState!.validate()) {
              _formkey.currentState!.save();
              issignin
                  ? context.read<SigningBloc>().add(SubmitSignIn())
                  : context.read<SigningBloc>().add(SubmitSignUp());
              print(
                  '${bloc.user.email}mmmmmmmmmmmmmmmmmmmmmmmmm${bloc.user.password}');
            }
            
          },
          style: Theme.of(context).elevatedButtonTheme.style,
          child: Text(
            issignin ? 'Sign-in' : 'Sign-up',
            style: Theme.of(context).textTheme.labelSmall,
          ),
        ),
        TextButton(
            onPressed: () => context.read<SigningBloc>().add(ChangeSignMode()),
            child: Text(
              issignin ? 'sign up instead' : 'sign in instead',
              style: Theme.of(context).textTheme.labelMedium,
            ))
      ],
    );
  }

  Widget buildtextformfield(BuildContext context, String label, IconData icon) {
    final bloc = BlocProvider.of<SigningBloc>(context);
    return TextFormField(
      controller: label == 'Password' ? _passwordcontroller : null,
      validator: (value) {
        switch (label) {
          case 'E-mail':
            if (value!.isEmpty || !value.contains('@')) {
              return 'invalid email';
            }

            break;

          case 'Password':
            if (value!.isEmpty || value.length < 6) {
              return 'Short password';
            }
            break;
          case 'Username':
            if (value!.isEmpty || value.length < 4) {
              return 'Username must be at least 4 characters';
            }
            break;
          default:
            null;
        }
      },
      onSaved: (newValue) {
        if (label == 'E-mail') {
          bloc.user.email = newValue!;
        }
        if (label == 'Password') {
          bloc.user.password = newValue!;
        } else {
          bloc.user.username = newValue!;
        }
      },
      decoration: InputDecoration(
          label: Text(label),
          icon: Icon(
            icon,
            color: Theme.of(context).primaryColor,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: Theme.of(context).primaryColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(color: Theme.of(context).primaryColor),
          )),
    );
  }
}
