part of 'signing_bloc.dart';

@immutable
abstract class SigningEvent {}
class SubmitSignIn extends SigningEvent{}
class SubmitSignUp extends SigningEvent{}
class ChangeSignMode extends SigningEvent{}
class Logout extends SigningEvent{}



