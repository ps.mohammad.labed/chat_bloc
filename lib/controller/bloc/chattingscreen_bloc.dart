import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../model/db_helper.dart';
import 'signing_bloc.dart';

part 'chattingscreen_event.dart';
part 'chattingscreen_state.dart';

class ChattingscreenBloc
    extends Bloc<ChattingscreenEvent, ChattingscreenState> {
  final signbloc = SigningBloc();
String senderid='';
   bool isme = false;
 String message = '';
 String id='';
  final db = DBHelper();
  ChattingscreenBloc() : super(ChattingscreenInitial()) {
    on<SendTheMessage>((event, emit) async {
      print('-----------------------------------------------');
      try {
        final pref=await SharedPreferences.getInstance();
       final data= jsonDecode(pref.getString('userdata')!);
       id=data['userid']!.toString();
        String useremail=data['useremail']!.toString();
       
        await db.sendmessage(message, id, useremail);
        emit(MessageSent());
      } catch (e) {
        emit(ErrorSending());
      }
    });
    
   
   
  }
 Stream<QuerySnapshot<Map<String,dynamic>>>  getmessages(){
    return db.getmsg();
  }
}
