part of 'chattingscreen_bloc.dart';

@immutable
abstract class ChattingscreenState {}

class ChattingscreenInitial extends ChattingscreenState {}

class MessageSent extends ChattingscreenState {}

class ErrorSending extends ChattingscreenState {}
class Loading extends ChattingscreenState{}
class Feched extends ChattingscreenState{}

