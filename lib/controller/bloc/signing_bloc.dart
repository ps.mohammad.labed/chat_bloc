// ignore_for_file: avoid_print

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../model/db_helper.dart';
import '../../model/user_data.dart';

part 'signing_event.dart';
part 'signing_state.dart';

class SigningBloc extends Bloc<SigningEvent, SigningState> {
UserData user=UserData('', '', '', '');
  bool issignin = false;
  String errormsg = 'An Error Occured';
  String id='';

  final db = DBHelper();
  SigningBloc() : super(SigningInitial()) {
    on<ChangeSignMode>((event, emit) {
      emit(Changed());
      issignin = !issignin;
    });
    on<SubmitSignUp>((event, emit) async {
      emit(SigningLoading());
      try {
       user.id=
            await db.authuser(user.email, user.password, user.username, 'signUp');
              id=await db.getid();
        emit(SigningSuccess());
      } catch (e) {
        emit(SigningFailed());
        if (e.toString().contains('EMAIL_EXISTS')) {
          errormsg = 'This email address is already in use.';
        }
        if (e.toString().contains('INVALID_EMAIL')) {
          errormsg = 'This is not a valid email address';
        }
        if (e.toString().contains('WEAK_PASSWORD')) {
          errormsg = 'This password is too weak.';
        }
        if (e.toString().contains('EMAIL_NOT_FOUND')) {
          errormsg = 'Could not find a user with that email.';
        }
        if (toString().contains('INVALID_PASSWORD')) {
          errormsg = 'Invalid password.';
        }

        print('$e 111111111111111111');
      }
    });
    on<SubmitSignIn>((event, emit) async {
      emit(SigningLoading());
      try {
      user.id=  await db.authuser(user.email, user.password, user.username, 'signInWithPassword');
      id=await db.getid();
      
        emit(SigningSuccess());
      } catch (e) {
        emit(SigningFailed());
        if (e.toString().contains('EMAIL_EXISTS')) {
          errormsg = 'This email address is already in use.';
        }
        if (e.toString().contains('INVALID_EMAIL')) {
          errormsg = 'This is not a valid email address';
        }
        if (e.toString().contains('WEAK_PASSWORD')) {
          errormsg = 'This password is too weak.';
        }
        if (e.toString().contains('EMAIL_NOT_FOUND')) {
          errormsg = 'Could not find a user with that email.';
        }
        if (toString().contains('INVALID_PASSWORD')) {
          errormsg = 'Invalid password.';
        }

        print('$e 111111111111111111');
      }
    });
    on<Logout>((event, emit) async{
      try{
   emit(SigningLoading());
await db.logout();
emit(LogedOut());
      }
      catch(e){
        emit(SigningFailed());
      }
   

    });
   
  }
}
