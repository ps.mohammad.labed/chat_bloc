part of 'chattingscreen_bloc.dart';

@immutable
abstract class ChattingscreenEvent {}

class SendTheMessage extends ChattingscreenEvent {}


class GetMessages extends ChattingscreenEvent{}
