part of 'signing_bloc.dart';

@immutable
abstract class SigningState {}

class SigningInitial extends SigningState {}

class SigningLoading extends SigningState {}

class SigningSuccess extends SigningState {}

class SigningFailed extends SigningState {}

class LogedOut extends SigningState {}
class Changed extends SigningState {}
