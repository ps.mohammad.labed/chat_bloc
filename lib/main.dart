import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'controller/bloc/chattingscreen_bloc.dart';
import 'view/chatting_screen.dart';
import 'controller/bloc/signing_bloc.dart';

import 'view/sign_screen.dart';

void main()async {
WidgetsFlutterBinding.ensureInitialized();
await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => SigningBloc(),
        ),
        BlocProvider<ChattingscreenBloc>(
                create: (context) => ChattingscreenBloc(),
               
              )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          textTheme: const TextTheme(
              labelSmall: TextStyle(color: Colors.white),
              labelMedium:
                  TextStyle(color: Colors.teal, fontWeight: FontWeight.bold)),
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.teal),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  ))),
          primarySwatch: Colors.teal,
        ),
        home: const SignScreen(),
        routes: {
          ChattingScreen.routename: (context) =>const ChattingScreen(),
        },
      ),
    );
  }
}
